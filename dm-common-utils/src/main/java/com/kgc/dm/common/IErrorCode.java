package com.kgc.dm.common;

public interface IErrorCode {
    public String getErrorCode();
    public String getErrorMessage();
}
