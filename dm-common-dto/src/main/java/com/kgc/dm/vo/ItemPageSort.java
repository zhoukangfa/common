package com.kgc.dm.vo;

import java.util.Date;

public class ItemPageSort {
    private Long itemType1Id;
    private Long itemType2Id;
    private Integer areaId;//城市id
    private Date startTime;
    private Date endTime;
    private String sort;//排序规则"recommend"："推荐","recentShow":"最近演出","recentSell"：最近上架

    public Long getItemType1Id() {
        return itemType1Id;
    }

    public void setItemType1Id(Long itemType1Id) {
        this.itemType1Id = itemType1Id;
    }

    public Long getItemType2Id() {
        return itemType2Id;
    }

    public void setItemType2Id(Long itemType2Id) {
        this.itemType2Id = itemType2Id;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
