package com.kgc.dm.mapper;
import com.kgc.dm.common.Page;
import com.kgc.dm.pojo.DmItem;
import com.kgc.dm.vo.DmFloorItems;
import com.kgc.dm.vo.ItemPageSort;
import com.kgc.dm.vo.ItemSearchVo;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DmItemMapper {

	public DmItem getDmItemById(@Param(value = "id") Long id)throws Exception;

	public List<DmItem>	getDmItemListByMap(Map<String,Object> param)throws Exception;

	public Integer getDmItemCountByMap(Map<String,Object> param)throws Exception;

	public Integer insertDmItem(DmItem dmItem)throws Exception;

	public Integer updateDmItem(DmItem dmItem)throws Exception;

	public Integer deleteDmItemById(@Param(value = "id") Long id)throws Exception;

	public Integer batchDeleteDmItem(Map<String,List<String>> params);

	public List<DmFloorItems> queryItemByFloor()throws Exception;

	public List<ItemSearchVo> queryItemSearchPage(ItemPageSort itemPageSort) throws Exception;

}
