package com.kgc.dm.client;

import com.kgc.dm.config.DmConfiguration;
import com.kgc.dm.fallback.DmSearchClientFallBack;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

@FeignClient(name = "dm-search-provider", configuration = DmConfiguration.class,fallback = DmSearchClientFallBack.class)
public interface RestDmSearchClient {

    @RequestMapping(value = "/getconnection")
    public void getconnection()throws Exception;

    @RequestMapping(value = "/addIndex")
    public void addIndex() throws Exception;

    @RequestMapping(value = "/addDocument")
    public void addDocument() throws Exception;

    @RequestMapping(value = "close")
    public void close();


}

